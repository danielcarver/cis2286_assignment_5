<?php
/**
* Author: Dan Carver
* Date: 2017-02-27
* Purpose: Assignment 5. The purpose of this web application is to display the lyrics to a song, and the date and time
* to the user.
*/

//vars
$song = file_get_contents("song.txt");
$title = "`some people's lives`";
$songTitle = "";
$mostUsedWord = "";
$lyrics = "";
$lyric = "";
$sunlight = "sunlight";
$everything = "everything";
$highestCount = 0;
date_default_timezone_set("America/Toronto");
$date = date("g:ia") . " on " . date("M j, Y T");
?>
<!doctype html>
<html lang="en">
    <head>
        <title>Lyrics</title>
        <link rel="stylesheet" href="styles.css" type="text/css">
    </head>
    <body>
        <div id="lyricsArea">
            <?php
            //break lyrics up with br's
            $lyric = nl2br($song);

            //remove the title from the lyrics
            $title = substr($lyric, 0, 27);
            $songTitle = trim($title,"`");
            $lyrics = str_replace($title, "", $lyric);

            //if the word sunlight exists make it yellow with a green background
            if(strpbrk($lyrics, $sunlight))
            {
                $replaceSunlight = $sunlight;
                $lyrics = str_replace($sunlight, "<span style='background-color: green'><span style='color: yellow'>" . $replaceSunlight . "</span></span>", $lyrics);
            }

            //if the word everything exists make its font size 30px
            if(strpbrk($lyrics, $everything))
            {
                $replaceEverything = $everything;
                $lyrics = str_replace($everything, "<span style='font-size: 30px'>" . $replaceEverything . "</span>", $lyrics);
            }

            //display the song title
            echo "<h1>$songTitle</h1>";
            ?>
            <!--this photo is a link to the Bette Midler website-->
            <!--http://bettemidler.com/wp-content/uploads/2016/04/90_some-peoples-lives.jpg-->
            <a href="http://bettemidler.com/">
            <img src="photo/90somePeoplesLives.jpg" id="albumArt" alt="Bettemidler.com">
            </a>
            <?php
            //displaying the song's lyrics
            echo $lyrics;

            //ez pz remove the tags from the lyrics so <br /> tags do not count
            $lyrics = strip_tags($lyrics);

            //convert the string to lower case so you don't have 'yes' and 'Yes'
            $lyrics = strtolower($lyrics);

            //turn the lyrics into an associative array, figure out which key (word) has the highest value (# of times it appears)
            $words = str_word_count($lyrics, 1);
            $values = (array_count_values($words));
            foreach($values as $value => $val)
            {
                if($val > $highestCount)
                {
                    $highestCount = $val;
                    $mostUsedWord = $value;
                }
            }

            ?>
            <h3>Character count: <?php echo strlen($lyrics); ?></h3>
            <h3>Word count: <?php echo str_word_count($lyrics); ?></h3>
            <h3>Most used word: <?php echo "$mostUsedWord, counted $highestCount times"; ?></h3>
            <?php
            echo "It is currently $date";
            ?>
        </div>
    </body>
</html>
